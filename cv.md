![Pic.1.jpg](./Pic.1.jpg)

# Maryna Mukhoid


## My Contact Info

* Address: Ukraine, Kiev
* Phone: +994 706371716
* E-mail: ginger.cat.10520@gmail.com
* GitHub: MMV1984

## Summary

I’m currently working several small web projects. My goal is to learn everything new and exciting. I love to code and coffee. I can(love) spend a lot of time doing what interests me. My core strengths are in problem solving and fast learning.I want to get knowledge and skills that will be enough for employment in a company.

## Skills

* HTML

* CSS (Framework Bootstrap, Preprocessor SCSS, BEM methodology).

* JavaScript (Fundamentals,Functional Programming, OOP, Asynchronous JavaScript, ES6+,DOM),JSON.

* React JS, Redux (intermediate level knowledge).

* Version control: Git (remote service GitHub).

* Module Bundlers: Gulp, Webpack.

* C (basic knowledge), Python(basic knowledge) - Flask Framework( basic knowledge), SQLite(basic knowledge).

* Windows OS, Linux(Ubuntu)

* Figma(for web development)

* Editors: Sublime, Brackets, VSCode, PyCharm community.

## Code examples
```
function{color:blue} findLongestWord(text) {
    let words = text.split(" ");
    let word = "";
    // for (let i = 0; i < words.length; i++) {
    //     if (word.length < words[i].length) {
    //         word = words[i];
    //     }
    // }
    for (let item of words) {
        if (word.length < item.length) {
            word = item;
        }
    }
    return (word);
}
const longestWord = findLongestWord("Lorem ipsum dolor sit 
amet, consectetur adipiscing elit. Praesent 
rutrum, quam sit amet semper tempus, velit 
nibh pellentesque dui, nec consectetur 
erat orci et libero. ");
console.log(longestWord);
 ```

## Education

* Education: NTUU "KPI".

 + Qualification: engineer of computer systems

* IT-City Academy

 + Basic knowledge of programming and algorithm knowledge using C language.

* Ecma-Code Education

+ Basic and advanced knowledge of üeb development knowledge using JavaScript and React JS.

* Pragmatech Education

+ Introduction to programming,Algorithms, Pseudocode, Syntax, Data Types, Operators. Booleans, CommandLine Interaction, Functions.

## Experience

* Small own business – Fast-food delivery(Since 2014).

* I have little experience in JS and Frontend development. I have worked in the team on several small projects and currently working.

## Languages

* Ukrainian

* Russian

* English

[StreamLine Language School English test result:](https://doka.guide "StreamLine Language School English") Pre-Intermediate (CEFR A2+).  

[EPAM English test result:](https://epam.com "EPAM")A1 I try to learn English in every possible way. I use application in smartphone: Duolingo and I use google translate a lot.


